#include <iostream>
#include <chrono>
#include <TGUI/TGUI.hpp>
#include "Load.h"
#include "Ticker.h"
#include "TextSlider.h"
#include <cmath>

using namespace std;

int main() {
  array<shared_ptr<sf::Texture>, 3> textures;
  for (auto &i:textures) {
    i = make_shared<sf::Texture>();
  }
  textures[0]->loadFromFile("../logo.png");
  textures[1]->loadFromFile("../logo1.png");
  textures[2]->loadFromFile("../logo2.png");

  sf::RenderWindow window(sf::VideoMode(800, 500), "My window");
  tgui::Gui gui(window);

  auto allLayout = tgui::VerticalLayout::create();

  auto settingsLayout = tgui::HorizontalLayout::create();

  auto displaySettingsLayout = tgui::VerticalLayout::create();

  auto massCenterLayout = tgui::HorizontalLayout::create();

  auto massCenterLabel = tgui::Label::create(L"Отображать центр масс");
  massCenterLabel->getSharedRenderer()->setTextColor(sf::Color::White);
  auto massCenter = tgui::CheckBox::create();
  massCenter->setChecked(true);

  massCenterLayout->add(massCenterLabel);
  massCenterLayout->add(massCenter, 0.3);

  auto speedVectorLayout = tgui::HorizontalLayout::create();

  auto speedVector = tgui::CheckBox::create();
  speedVector->setChecked(true);

  speedVectorLayout->add(tgui::Label::create(L"Отображать вектор скорости"));
  speedVectorLayout->add(speedVector, 0.3);

  auto gravityForceLayout = tgui::HorizontalLayout::create();

  auto gravityForce = tgui::CheckBox::create();
  gravityForce->setChecked(true);

  gravityForceLayout->add(tgui::Label::create(L"Отображать вектор силы тяжести"));
  gravityForceLayout->add(gravityForce, 0.3);

  auto dataLayout = tgui::HorizontalLayout::create();

  auto dataCheck = tgui::CheckBox::create();

  dataLayout->add(tgui::Label::create(L"Отображать данные системы"));
  dataLayout->add(dataCheck, 0.3);

  displaySettingsLayout->add(massCenterLayout);
  displaySettingsLayout->add(speedVectorLayout);
  displaySettingsLayout->add(gravityForceLayout);
  displaySettingsLayout->add(dataLayout);

  auto tickerSettingsLayout = tgui::VerticalLayout::create();

  auto rodLengthLayout = tgui::HorizontalLayout::create();

  auto rodLength = make_shared<TextSlider>(rodLengthLayout, L"Длина стержня", 100, 0.1,
                                           R"((([0-9]|([1-9][0-9])|(100))(\.[0-9]?)?)|(\.[0-9]?))", L"м");

  auto rodMassLayout = tgui::HorizontalLayout::create();

  auto rodMass = make_shared<TextSlider>(rodMassLayout, L"Масса стержня", 10, 0.01,
                                         R"((((10)|[0-9])(\.[0-9]?[0-9]?)?)|(\.[0-9]?[0-9]?))", L"кг");

  auto timeSpeedLayout = tgui::HorizontalLayout::create();

  auto timeSpeed = make_shared<TextSlider>(timeSpeedLayout, L"Скорость времени", 33.33, 0.03333,
                                           R"((([0-9]|([1-2][0-9])|(3[0-2]))(\.[0-9]?[0-9]?)?)|(33(\.(([0-2][0-9]?)|(3[0-3]?)))?)|(\.[0-9]?[0-9]?))",
                                           L"");

  auto densityLayout = tgui::HorizontalLayout::create();

  auto density = make_shared<TextSlider>(densityLayout, L"Плотность груза", 10, 0.01,
                                         R"((((10)|[0-9])(\.[0-9]?[0-9]?)?)|(\.[0-9]?[0-9]?))", L"г/(м^2)");

  auto velocityResetButton = tgui::Button::create(L"Сбросить скорость");

  tickerSettingsLayout->add(rodMassLayout);
  tickerSettingsLayout->add(rodLengthLayout);
  tickerSettingsLayout->add(timeSpeedLayout);
  tickerSettingsLayout->add(densityLayout);
  tickerSettingsLayout->add(velocityResetButton);

  auto loadSettingsLayout = tgui::VerticalLayout::create();

  auto angleLayout = tgui::HorizontalLayout::create();

  auto angle = make_shared<TextSlider>(angleLayout, L"Угол", 359, 1,
                                       R"([0-9]|([1-9][0-9])|([0-2][0-9][0-9])|(3[0-5][0-9]))", L"°");

  auto scaleLayout = tgui::HorizontalLayout::create();

  auto scale = make_shared<TextSlider>(scaleLayout, L"Масштаб", 1000, 1, R"([0-9]|([1-9][0-9])|([1-9][0-9][0-9]))",
                                       L"");

  auto tickerAngleLayout = tgui::HorizontalLayout::create();

  auto tickerAngle = make_shared<TextSlider>(tickerAngleLayout, L"Угол маятника", 359, 1,
                                             R"([0-9]|([1-9][0-9])|([0-2][0-9][0-9])|(3[0-5][0-9]))", L"°");

  auto loadMassCenterLayout = tgui::HorizontalLayout::create();

  auto loadMassCenterCheck = tgui::CheckBox::create();
  loadMassCenterCheck->setChecked(true);

  loadMassCenterLayout->add(tgui::Label::create(L"Отображать центр масс груза"));
  loadMassCenterLayout->add(loadMassCenterCheck, 0.3);

  loadSettingsLayout->add(angleLayout);
  loadSettingsLayout->add(scaleLayout);
  loadSettingsLayout->add(tickerAngleLayout);
  loadSettingsLayout->add(loadMassCenterLayout);

  auto loadLayout = tgui::VerticalLayout::create();

  auto texturesList = tgui::ComboBox::create();
  texturesList->addItem(L"ЮФУ", "0");
  texturesList->addItem(L"Физфак", "1");
  texturesList->addItem(L"тест", "2");

  auto loadCanvas = tgui::Canvas::create();
  auto load = make_shared<Load>();

  loadLayout->add(texturesList);
  loadLayout->add(loadCanvas, 7);

  settingsLayout->add(displaySettingsLayout, 1.5);
  settingsLayout->add(tickerSettingsLayout, 2);
  settingsLayout->add(loadSettingsLayout, 2);
  settingsLayout->add(loadLayout);

  allLayout->add(settingsLayout, 3);

  auto tickerCanvas = tgui::Canvas::create();
  Ticker ticker(load);

  allLayout->add(tickerCanvas, 7);

  gui.add(allLayout);

  rodMass->connect([&ticker](float val) {
    ticker.setRodMass(val);
  }, 4);

  rodLength->connect([&ticker](float val) {
    ticker.setRodLength(val);
  }, 5);

  timeSpeed->connect([&ticker](float val) {
    ticker.setTimeSpeed(val);
  }, 4);

  density->connect([&ticker](float val) {
    ticker.setDensity(val / 1000);
  }, 4);

  angle->connect([load](float val) {
    load->rotate(val);
  }, 7);

  scale->connect([load](float val) {

  }, 7);

  bool rotationSet = true;
  tickerAngle->connect([&ticker, &rotationSet](float val) {
    if (rotationSet) {
      rotationSet = false;
      ticker.setRotation(val * pi / 180);
      rotationSet = true;
    }
  }, 7);
  ticker.onRotation([&rotationSet, tickerAngle, &ticker] {
    if (rotationSet) {
      rotationSet = false;
      tickerAngle->setValue(int(180 * (ticker.getRotation() / pi + 40000)) % 360);
      rotationSet = true;
    }
  });

  cout << texturesList->onItemSelect.getName() << endl;

  texturesList->connect(texturesList->onItemSelect.getName(), [&textures, load](const string &name, const string &id) {
    if (id == "0") {
      load->setTexture(textures[0]);
    } else if (id == "1") {
      load->setTexture(textures[1]);
    } else if (id == "2") {
      load->setTexture(textures[2]);
    } else {
      throw logic_error("some err");
    }
  });

  loadCanvas->connect(loadCanvas->onClick.getName(), [load, loadCanvas](const sf::Vector2f &point) {
    load->setConnectPoint({point.x / loadCanvas->getSize().x, point.y / loadCanvas->getSize().y});
  });

  velocityResetButton->connect(velocityResetButton->onClick.getName(), [&ticker] {
    ticker.resetVelocity();
  });

  massCenter->connect(massCenter->onCheck.getName(), [&ticker] {
    ticker.showMassCenter(true);
  });

  massCenter->connect(massCenter->onUncheck.getName(), [&ticker] {
    ticker.showMassCenter(false);
  });

  loadMassCenterCheck->connect(loadMassCenterCheck->onCheck.getName(), [load] {
    load->showMassCenter(true);
  });

  loadMassCenterCheck->connect(loadMassCenterCheck->onUncheck.getName(), [load] {
    load->showMassCenter(false);
  });

  speedVector->connect(speedVector->onCheck.getName(), [&ticker] {
    ticker.showVelocityVector(true);
  });

  speedVector->connect(speedVector->onUncheck.getName(), [&ticker] {
    ticker.showVelocityVector(false);
  });

  gravityForce->connect(gravityForce->onCheck.getName(), [&ticker] {
    ticker.showGravityForce(true);
  });

  gravityForce->connect(gravityForce->onUncheck.getName(), [&ticker] {
    ticker.showGravityForce(false);
  });
  texturesList->setSelectedItemById("0");
  rodMass->setValue(1);
  rodLength->setValue(1);
  timeSpeed->setValue(10);
  density->setValue(1);
  angle->setValue(1);
  scale->setValue(1);
  massCenter->setChecked(false);
  loadMassCenterCheck->setChecked(false);
  speedVector->setChecked(false);
  gravityForce->setChecked(false);

  bool mousePressed = false;
  auto n = chrono::system_clock::now();
  while (window.isOpen()) {
    n = chrono::system_clock::now() + 17ms;
    sf::Event event{};
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed ||
          (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)) {
        window.close();
      } else if (event.type == sf::Event::Resized) {
        float windowWidth = event.size.width, windowHeight = event.size.height;
        auto view = window.getView();
        view.setSize(windowWidth, windowHeight);
        view.setCenter(windowWidth / 2, windowHeight / 2);
        gui.setView(view);
        window.setView(view);
        load->setSize(loadCanvas->getSize());
        ticker.setSize(tickerCanvas->getSize());
      } else if (event.type == sf::Event::MouseButtonPressed) {
        mousePressed = event.mouseButton.button == sf::Mouse::Button::Left;
      } else if (event.type == sf::Event::MouseButtonReleased) {
        mousePressed = false;
      } else if (event.type == sf::Event::MouseLeft) {
        mousePressed = false;
      } else if (event.type == sf::Event::MouseMoved && mousePressed) {
        auto point = sf::Vector2f(event.mouseMove.x, event.mouseMove.y) - loadCanvas->getAbsolutePosition();
        if (loadCanvas->mouseOnWidget(point + loadCanvas->getPosition())) {
          load->setConnectPoint({point.x / loadCanvas->getSize().x, point.y / loadCanvas->getSize().y});
        }
      }
      gui.handleEvent(event);
    }
    window.clear();
    gui.draw();
    loadCanvas->clear();
    loadCanvas->draw(*load);
    loadCanvas->display();
    tickerCanvas->clear();
    tickerCanvas->draw(ticker);
    tickerCanvas->display();
    window.display();
    ticker.update();
    if((n - chrono::system_clock::now()).count() < 0){
      //cout << "some err" << endl;
    }
    this_thread::sleep_until(n);
  }
  return 0;
}