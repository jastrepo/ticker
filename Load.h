//
// Created by nikita on 3/25/19.
//

#ifndef TICKER_LOAD_H
#define TICKER_LOAD_H

#include <SFML/Graphics.hpp>
#include "Vector.h"
#include "WatchVar.h"

class Load : public sf::Drawable {
  sf::Sprite sprite;
  sf::CircleShape connectDot, massCenterDot;
  shared_ptr<sf::Texture> texture;
  WatchVar<sf::Texture> currentTexture;
  WatchVar<double> mass, alpha;
  WatchVar<float> scale;
  WatchVar<Vector> connectPoint, massCenter, size, textureSize;

  void updateCurrentTexture();

  void updateMassAndItsCenter();

  void updateScale();

  void updateSprite();

  void updateMassCenterDot();

  void updateConnectDot();

  void init();

public:
  explicit Load(Vector newSize);

  Load();

  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  Vector getSize() const;

  void setSize(Vector newSize);

  void onConnectPointChange(function<void()> listener);

  Vector getConnectPoint() const;

  void setConnectPoint(Vector point);

  void onTextureChange(function<void()> listener);

  sf::Texture getTexture() const;

  void setTexture(const shared_ptr<sf::Texture> &newTexture);

  void onMassCenterChange(function<void()> listener);

  Vector getMassCenter() const;

  void onMassChange(function<void()> listener);

  double getMass() const;

  void rotate(double newAlpha);

  void onJChange(function<void()> listener);

  double getJRelative(Vector center);

  void showMassCenter(bool show);
};

#endif //TICKER_LOAD_H