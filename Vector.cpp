//
// Created by nikita on 3/25/19.
//
#include "Vector.h"
#include <cmath>

using namespace std;

Vector::Vector(double x, double y) : x(x), y(y) {}

Vector Vector::operator+(const Vector &second) const {
  return Vector(x + second.x, y + second.y);
}

Vector Vector::r(const double &alpha) const {
  double si = sin(alpha), co = cos(alpha);
  return Vector((co * x - si * y), (si * x + co * y));
}

Vector Vector::operator-(const Vector &second) const {
  return Vector(x - second.x, y - second.y);
}

Vector Vector::operator+(const double &second) const {
  return Vector(x + second, y + second);
}

Vector Vector::operator-(const double &second) const {
  return Vector(x - second, y - second);
}

Vector Vector::operator*(const double &second) const {
  return Vector(x * second, y * second);
}

Vector Vector::operator/(const double &second) const {
  return Vector(x / second, y / second);
}

Vector::operator sf::Vector2f() const {
  return sf::Vector2f(float(x), float(y));
}

Vector::Vector(const sf::Vector2f &second) : x(second.x), y(second.y) {}

Vector Vector::operator*(const Vector &second) const {
  return Vector(x * second.x, y * second.y);
}

Vector::Vector(const tgui::Vector2f &second) : x(second.x), y(second.y) {}

Vector::operator tgui::Vector2f() const {
  return tgui::Vector2f(float(x), float(y));
}
