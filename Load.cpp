//
// Created by nikita on 3/25/19.
//
#include "Load.h"
#include <cmath>
#include <iostream>

using namespace std;

void Load::updateCurrentTexture() {
  const auto &cSize = texture->getSize();
  float sizeX = cSize.x, sizeY = cSize.y;
  sf::RenderTexture render;
  render.create(cSize.x, cSize.y);
  sf::Sprite cSprite(*texture);
  auto k = float((sizeX > sizeY ? sizeY / sizeX : sizeX / sizeY) / sqrt(2));
  cSprite.setScale(k, k);
  float imageCenterX = sizeX / 2, imageCenterY = sizeY / 2;
  cSprite.setOrigin(imageCenterX, imageCenterY);
  cSprite.setPosition(imageCenterX, imageCenterY);
  cSprite.setRotation(float(alpha));
  render.draw(cSprite);
  render.display();
  if (currentTexture->getSize().x == sizeX && currentTexture->getSize().y == sizeY) {
    currentTexture.modify([&render](sf::Texture &text) {
      text.update(render.getTexture());
    });
  } else {
    currentTexture = render.getTexture();
  }
  textureSize = Vector(sizeX, sizeY);
}

void Load::updateMassAndItsCenter() {
  const auto &image = currentTexture->copyToImage();
  auto maxX = unsigned(textureSize->x), maxY = unsigned(textureSize->y);
  double sumX, sumY, m, cMass;
  cMass = sumX = sumY = 0;
  for (unsigned i = 0; i < maxX; i++) {
    for (unsigned j = 0; j < maxY; j++) {
      m = image.getPixel(i, j).a / 255.;
      cMass += m;
      sumX += m * i;
      sumY += m * j;
    }
  }
  massCenter = {sumX / cMass, sumY / cMass};
  mass = cMass;
}

void Load::updateScale() {
  scale = min(size->x / textureSize->x, size->y / textureSize->y);
}

void Load::updateSprite() {
  sprite.setScale(scale, scale);
}

void Load::updateMassCenterDot() {
  massCenterDot.setPosition(*massCenter * scale - 3);
}

void Load::updateConnectDot() {
  connectDot.setPosition(*connectPoint * scale - 3);
}

void Load::init() {
  texture = nullptr;
  connectDot.setRadius(3);
  massCenterDot.setRadius(3);
  massCenterDot.setFillColor(sf::Color::Green);
  connectPoint.on([this] {
    updateConnectDot();
  });
  size.on([this] {
    updateScale();
  });
  scale.on([this] {
    updateSprite();
    updateMassCenterDot();
    updateConnectDot();
  });
  textureSize.on([this] {
    updateScale();
  });
  currentTexture.on([this] {
    if (sprite.getTexture() == nullptr || sprite.getTexture() != &*currentTexture) {
      sprite = sf::Sprite(*currentTexture);
      updateSprite();
    }
    updateMassAndItsCenter();
  });
  massCenter.on([this] {
    updateMassCenterDot();
  });
  alpha.on([this] {
    updateCurrentTexture();
  });
}

Load::Load(Vector newSize) : size(newSize) {
  init();
}

Load::Load() {
  init();
}

void Load::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  target.draw(sprite, states);
  target.draw(massCenterDot, states);
  target.draw(connectDot, states);
}

Vector Load::getSize() const {
  return size;
}

void Load::setSize(Vector newSize) {
  size = newSize;
}

void Load::onConnectPointChange(function<void()> listener) {
  connectPoint.on(listener);
}

Vector Load::getConnectPoint() const {
  return connectPoint;
}

void Load::setConnectPoint(Vector point) {
  connectPoint = point * size / scale;
}

void Load::onTextureChange(function<void()> listener) {
  currentTexture.on(listener);
}

sf::Texture Load::getTexture() const {
  return *currentTexture;
}

void Load::setTexture(const shared_ptr<sf::Texture> &newTexture) {
  texture = newTexture;
  updateCurrentTexture();
}

void Load::onMassCenterChange(function<void()> listener) {
  massCenter.on(listener);
}

Vector Load::getMassCenter() const {
  return massCenter;
}

void Load::onMassChange(function<void()> listener) {
  mass.on(listener);
}

double Load::getMass() const {
  return mass;
}

void Load::rotate(double newAlpha) {
  alpha = newAlpha;
}

void Load::onJChange(function<void()> listener) {
  currentTexture.on(listener);
  connectPoint.on(listener);
}

double Load::getJRelative(Vector center) {
  const auto &image = currentTexture->copyToImage();
  auto maxX = image.getSize().x, maxY = image.getSize().y;
  const auto &c = center - connectPoint;
  double J, x, y;
  J = 0;
  for (unsigned i = 0; i < maxX; i++) {
    for (unsigned j = 0; j < maxY; j++) {
      x = i + c.x, y = j + c.y;
      J += image.getPixel(i, j).a / 255. * (x * x + x * 1 + 1 * 1 / 3. + y * y + y * 1 + 1 * 1 / 3.);
    }
  }
  return J;
}

void Load::showMassCenter(bool show) {
  massCenterDot.setFillColor(show ? sf::Color::Green : sf::Color::Transparent);
}
