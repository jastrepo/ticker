//
// Created by nikita on 3/27/19.
//

#include "TextSlider.h"
#include <iostream>

TextSlider::TextSlider(shared_ptr<tgui::HorizontalLayout> layout, const wstring &name, float max, float step,
                       const string &regex, const wstring &dim) {

  slider = tgui::Slider::create(0, max);
  slider->setStep(step);
  edit = tgui::EditBox::create();
  edit->setInputValidator(regex);

  layout->add(tgui::Label::create(name));
  layout->add(slider);
  layout->add(edit);
  layout->add(tgui::Label::create(dim), 0.5);

}

void TextSlider::connect(function<void(float)> action, int signs) {
  slider->connect(slider->onValueChange.getName(), [this, action, signs](float a) {
    action(a);
    if (set) {
      set = false;
      const auto &str = to_string(a);
      edit->setText(str.substr(0, str.size() - signs));
      set = true;
    }
  });
  edit->connect(edit->onTextChange.getName(), [this](const string &s) {
    if (set) {
      set = false;
      if (s.empty()) {
        slider->setValue(0);
      } else {
        slider->setValue(stof(s));
      }
      set = true;
    }
  });

}

void TextSlider::setValue(float val) {
  slider->setValue(val);
}
