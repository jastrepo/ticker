//
// Created by nikita on 3/27/19.
//
#ifndef TICKER_TEXTSLIDER_H
#define TICKER_TEXTSLIDER_H

#include <TGUI/TGUI.hpp>

using namespace std;

class TextSlider {
  shared_ptr<tgui::Slider> slider;
  shared_ptr<tgui::EditBox> edit;
  bool set = true;
public:
  TextSlider(shared_ptr<tgui::HorizontalLayout> layout, const wstring &name, float max, float step, const string &regex,
             const wstring &dim);

  void connect(function<void(float)> setter, int signs);

  void setValue(float val);
};

#endif //TICKER_TEXTSLIDER_H