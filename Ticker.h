//
// Created by nikita on 3/26/19.
//
#ifndef TICKER_TICKER_H
#define TICKER_TICKER_H

#include "Load.h"
#include <chrono>
#include <cmath>
#include <mutex>
#include <atomic>
#include <future>

constexpr auto pi = 4 * atan(1);

class Ticker : public sf::Drawable {
  sf::Text text;
  sf::Sprite sprite;
  sf::CircleShape centerDot, massDot;
  std::array<sf::Vertex, 6> line;

  shared_ptr<Load> load;
  shared_ptr<sf::Font> font;
  sf::Texture texture;
  mutex access;
  atomic<bool> working;
  future<void> stop;
  WatchVar<Vector> center, connect, size, massCenter;
  Vector staticMassCenter, oldStaticMassCenter;
  WatchVar<double> oldAlpha, oldVelocity;
  WatchVar<double> rodLength, rodMass, mass, J, density;
  double alpha, velocity, oldMass, oldJ, oldSpeed, innerTime = 0, oldInnerTime;
  chrono::system_clock::time_point n;
  double speed = 0;
  bool showMass = true, showVelocity = true, showGravity = true, reset = false, setAlpha = false;

  void updateCenter();

  void updateCenterDot();

  void updateConnect();

  void updateConnectPoint();

  void updateSprite();

  void updateMassCenter();

  void updateMass();

  void updateMassDot();

  void updateJ();

  void updateSpeed();

  void updateGravity();

  void init();

public:
  Ticker(shared_ptr<Load> newLoad, Vector newSize);

  explicit Ticker(shared_ptr<Load> newLoad);

  ~Ticker() override;

  void update();

  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  void setSize(Vector newSize);

  void setRodLength(double length);

  void setRodMass(double newMass);

  void setTimeSpeed(double newSpeed);

  void onRotation(function<void()> listener);

  double getRotation() const;

  void setDensity(double newDensity);

  void setRotation(double newAlpha);

  void resetVelocity();

  void showMassCenter(bool show);

  void showVelocityVector(bool show);

  void showGravityForce(bool show);
};

#endif //TICKER_TICKER_H