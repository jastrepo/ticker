//
// Created by nikita on 3/26/19.
//

#include "Ticker.h"
#include <iostream>

void Ticker::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  target.draw(sprite, states);
  target.draw(centerDot, states);
  target.draw(massDot, states);
  target.draw(line.data(), line.size(), sf::Lines, states);
  target.draw(text, states);
}

Ticker::Ticker(shared_ptr<Load> newLoad, Vector newSize) : load(move(newLoad)) {
  init();
  size = newSize;
}

void Ticker::update() {
  lock_guard<mutex> guard(access);
  oldMass = mass;
  oldJ = J;
  oldSpeed = speed;
  if (setAlpha) {
    alpha = oldAlpha;
    setAlpha = false;
  } else {
    oldAlpha = alpha;
  }
  if (reset) {
    velocity = 0;
    reset = false;
  }
  oldVelocity = velocity;
  oldStaticMassCenter = staticMassCenter;
  oldInnerTime = innerTime;
}

void Ticker::updateCenter() {
  center = *size / 2;
}

void Ticker::setSize(Vector newSize) {
  size = newSize;
}

void Ticker::updateCenterDot() {
  line[0].position = *center;
  centerDot.setPosition(*center - 10);
}

void Ticker::setRodLength(double length) {
  rodLength = length;
}

void Ticker::updateConnect() {
  connect = *center + Vector(-sin(oldAlpha), cos(oldAlpha)) * *rodLength;
}

void Ticker::updateConnectPoint() {
  line[1].position = *connect;
}

void Ticker::updateSprite() {
  sprite.setPosition(*connect);
  sprite.setOrigin(load->getConnectPoint());
  sprite.setRotation(oldAlpha * 180 / pi);
}

void Ticker::updateMassCenter() {
  if (abs(mass) > 1e-10) {
    const auto &rodCenter = Vector(0, rodLength);
    staticMassCenter = (rodCenter * rodMass / 2 +
                        (rodCenter - load->getConnectPoint() + load->getMassCenter()) * load->getMass() * density) /
                       mass;
    massCenter = staticMassCenter.r(oldAlpha);
  }
}

void Ticker::setRodMass(double newMass) {
  rodMass = newMass;
}

void Ticker::updateMass() {
  mass = rodMass + load->getMass() * density;
}

void Ticker::updateMassDot() {
  if (showMass) {
    massDot.setPosition(*massCenter + *center - 3);
  }
}

void Ticker::updateJ() {
  J = rodMass * rodLength * rodLength / 4 + load->getJRelative({0, rodLength}) * density;

}

void Ticker::init() {
  font = make_shared<sf::Font>();
  font->loadFromFile("/usr/share/fonts/TTF/DejaVuSans.ttf");
  text = sf::Text("", *font);
  text.setPosition(10, 10);
  centerDot.setRadius(10);
  massDot.setRadius(3);
  oldAlpha = 0;
  oldVelocity = 0;
  load->onTextureChange([this] {
    texture = load->getTexture();
    sprite = sf::Sprite(texture);
    updateSprite();
  });
  n = chrono::system_clock::now();
  size.on([this] {
    updateCenter();
  });
  center.on([this] {
    updateCenterDot();
    updateConnect();
    updateSpeed();
    updateGravity();
  });
  rodLength.on([this] {
    updateConnect();
    updateJ();
    updateMassCenter();
  });
  oldAlpha.on([this] {
    updateConnect();
    updateSprite();
    updateMassCenter();
    updateSpeed();
    updateGravity();
  });
  connect.on([this] {
    updateConnectPoint();
    updateSprite();
    updateMassCenter();
  });
  load->onConnectPointChange([this] {
    updateSprite();
    updateMassCenter();
  });
  rodMass.on([this] {
    updateMassCenter();
    updateMass();
    updateJ();
  });
  load->onMassCenterChange([this] {
    updateMassCenter();
  });
  load->onMassChange([this] {
    updateMassCenter();
    updateMass();
    updateJ();
  });
  massCenter.on([this] {
    updateMassDot();
    updateSpeed();
    updateGravity();
  });
  mass.on([this] {
    updateMassCenter();
    updateGravity();
  });
  load->onJChange([this] {
    updateJ();
  });
  density.on([this] {
    updateMass();
    updateMassCenter();
    updateJ();
  });
  oldVelocity.on([this] {
    updateSpeed();
  });
  working = true;
  promise<void> promiseToStop;
  stop = promiseToStop.get_future();
  thread([this](promise<void> &&promiseToStop) {
    while (working) {
      lock_guard<mutex> guard(access);
      double step = chrono::duration<double>(chrono::system_clock::now() - n).count() * oldSpeed;
      innerTime += step;
      n = chrono::system_clock::now();
      alpha += velocity * step;
      if (abs(oldJ) > 1e-10) {
        velocity += oldStaticMassCenter.r(alpha).x * oldMass * 10 / oldJ * step;
      }
      if (alpha > 1000 || alpha < -1000) {
        double alphaVal = alpha / 2 / pi;
        alphaVal -= int(alphaVal);
        if (alphaVal < 0) {
          alphaVal++;
        }
        alpha = alphaVal * 2 * pi;
      }
    }
    promiseToStop.set_value();
  }, move(promiseToStop)).detach();
}

Ticker::Ticker(shared_ptr<Load> newLoad) : load(move(newLoad)) {
  init();
}

void Ticker::setTimeSpeed(double newSpeed) {
  speed = newSpeed;
}

void Ticker::onRotation(function<void()> listener) {
  oldAlpha.on(listener);
}

double Ticker::getRotation() const {
  return oldAlpha;
}

void Ticker::setRotation(double newAlpha) {
  oldAlpha = newAlpha;
  setAlpha = true;
}

void Ticker::setDensity(double newDensity) {
  density = newDensity;
}

void Ticker::resetVelocity() {
  reset = true;
}

void Ticker::showMassCenter(bool show) {
  showMass = show;
  massDot.setFillColor(show ? sf::Color::Blue : sf::Color::Transparent);
}

void Ticker::updateSpeed() {
  if (showVelocity) {
    line[2].position = *massCenter + *center;
    line[3].position = *massCenter - Vector(cos(oldAlpha), sin(oldAlpha)) * oldVelocity * 100 + *center;
  }
}

void Ticker::showVelocityVector(bool show) {
  showVelocity = show;
  line[2].color = (show ? sf::Color::Yellow : sf::Color::Transparent);
  line[3].color = (show ? sf::Color::Green : sf::Color::Transparent);
}

void Ticker::updateGravity() {
  if (showGravity) {
    line[4].position = *massCenter + *center;
    line[5].position = *massCenter + Vector(0, 1) * mass * 10 + *center;
  }
}

void Ticker::showGravityForce(bool show) {
  showGravity = show;
  line[4].color = (show ? sf::Color::Blue : sf::Color::Transparent);
  line[5].color = (show ? sf::Color::Red : sf::Color::Transparent);
}

Ticker::~Ticker() {
  working = false;
  stop.get();
}
