//
// Created by nikita on 3/26/19.
//
#ifndef TICKER_WATCHVAR_H
#define TICKER_WATCHVAR_H

#include <functional>
#include <list>

using namespace std;

template<typename T>
class WatchVar {
  T val;
  list <function<void()>> listeners;
public:
  explicit WatchVar(const T &newVal);

  explicit WatchVar(T &&newVal);

  WatchVar() = default;

  const T *operator->() const;

  operator T() const; // NOLINT(google-explicit-constructor)

  const T &get() const;

  const T &operator*() const;

  T copy() const;

  void set(const T &newVal);

  void moveFrom(T &&newVal);

  WatchVar &operator=(const T &newVal);

  WatchVar &operator=(T &&newVal);

  void on(const function<void()> &listener);

  void modify(const function<void(T&)> action);
};

template<typename T>
void WatchVar<T>::on(const function<void()> &listener) {
  listeners.push_back(listener);
}

template<typename T>
WatchVar<T> &WatchVar<T>::operator=(const T &newVal) {
  set(newVal);
  return *this;
}

template<typename T>
WatchVar<T>::operator T() const {
  return get();
}

template<typename T>
WatchVar<T>::WatchVar(const T &newVal) {
  val = newVal;
}

template<typename T>
const T &WatchVar<T>::get() const {
  return val;
}

template<typename T>
void WatchVar<T>::set(const T &newVal) {
  val = newVal;
  for (const auto &listener:listeners) {
    listener();
  }
}

template<typename T>
T WatchVar<T>::copy() const {
  return val;
}

template<typename T>
WatchVar<T>::WatchVar(T &&newVal) {
  val = newVal;
}

template<typename T>
void WatchVar<T>::moveFrom(T &&newVal) {
  swap(val, newVal);
  for (const auto &listener:listeners) {
    listener();
  }
}

template<typename T>
const T *WatchVar<T>::operator->() const {
  return &val;
}

template<typename T>
WatchVar<T> &WatchVar<T>::operator=(T &&newVal) {
  moveFrom(move(newVal));
  return *this;
}

template<typename T>
const T &WatchVar<T>::operator*() const {
  return get();
}

template<typename T>
void WatchVar<T>::modify(const function<void(T &)> action) {
  action(val);
  for (const auto &listener:listeners) {
    listener();
  }
}

#endif //TICKER_WATCHVAR_H