//
// Created by nikita on 3/25/19.
//
#ifndef TICKER_POINT_H
#define TICKER_POINT_H

#include <TGUI/Vector2f.hpp>

class Vector {
public:
  double x, y;

  Vector(double x = 0, double y = 0); // NOLINT(google-explicit-constructor)

  Vector &operator=(const Vector &second) = default;

  Vector operator+(const Vector &second) const;

  Vector operator-(const Vector &second) const;

  Vector operator*(const Vector &second) const;

  Vector operator+(const double &second) const;

  Vector operator-(const double &second) const;

  Vector operator*(const double &second) const;

  Vector operator/(const double &second) const;

  Vector r(const double &alpha) const;

  operator sf::Vector2f() const; // NOLINT(google-explicit-constructor)

  Vector(const sf::Vector2f &second); // NOLINT(google-explicit-constructor)

  operator tgui::Vector2f() const; // NOLINT(google-explicit-constructor)

  Vector(const tgui::Vector2f &second); // NOLINT(google-explicit-constructor)
};

#endif //TICKER_POINT_H